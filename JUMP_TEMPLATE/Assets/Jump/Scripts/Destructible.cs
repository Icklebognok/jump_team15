﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public bool bFrozen = false;
    public bool bFloating = false;

    Rigidbody2D rb;
    SpriteRenderer sprite;
    Color defaultColor;
    public Color FrozenColor;
    public float defaultMass;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        defaultColor = sprite.color;
        defaultMass = rb.mass;
    }

    void Update()
    {
        if (bFrozen)
        {
            bFloating = false;
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            rb.gravityScale = 0;
            sprite.color = FrozenColor;
            rb.mass = defaultMass * 10;
        }
        else
        {
            rb.gravityScale = 1;
            sprite.color = defaultColor;
            rb.mass = defaultMass;
        }
    }
}
