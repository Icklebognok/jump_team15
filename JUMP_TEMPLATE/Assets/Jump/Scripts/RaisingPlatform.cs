﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaisingPlatform : MonoBehaviour
{
    public GameObject LinkedWheel;
    Cogwheel cogwheel;
    public float raiseSpeed;

    public float minimum;
    public float maximum;
    void Start()
    {
        cogwheel = LinkedWheel.GetComponent<Cogwheel>();
 
    }
    void Update()
    {
        if(cogwheel.bBeingPushed)
        {
            // Lower the platform
            if (transform.position.y > minimum)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - (raiseSpeed * Time.deltaTime));
            }

        } else if (cogwheel.bBeingPulled)
        {
            // Raise the platform
            if (transform.position.y < maximum)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y + (raiseSpeed * Time.deltaTime));
            }
        }
    }
}
