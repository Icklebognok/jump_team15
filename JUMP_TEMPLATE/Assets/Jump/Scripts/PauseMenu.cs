﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;
    public GameObject PauseUI;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(isPaused){
                Resume();
            }else{
                Pause();
            }
        }
        if(Input.GetKeyDown(KeyCode.R)){
            restart();
        }
    }
    public void Resume(){
        PauseUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;

    }
    void Pause(){
        PauseUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
    public void restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Resume();
    }
    public void options(){

    }
    public void quit(){
        Time.timeScale = 1f;
        isPaused = false;
        SceneManager.LoadScene("MainScene");
    }
}
