﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperAim : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> targets = new List<GameObject>();
    public Material defaultMaterial;
    public Material OutlineMaterial;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() && col.gameObject.layer != LayerMask.NameToLayer("Environment"))
        {
           SpriteRenderer render = col.gameObject.GetComponent<SpriteRenderer>();
<<<<<<< .merge_file_a05976
           Texture cachetexture = render.material.GetTexture("_Normal");
           render.material = OutlineMaterial;
           render.material.SetTexture("_Normal",cachetexture);
=======
           Texture2D cacheNormal = (Texture2D)render.material.GetTexture("_Normal");
           render.material = OutlineMaterial;
           render.material.SetTexture("_Normal", cacheNormal);
>>>>>>> .merge_file_a07972
            targets.Add(col.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() && col.gameObject.layer != LayerMask.NameToLayer("Environment"))
        {
            SpriteRenderer render = col.gameObject.GetComponent<SpriteRenderer>();
<<<<<<< .merge_file_a05976
            Texture cachetexture = render.material.GetTexture("_Normal");
           render.material = defaultMaterial;
           render.material.SetTexture("_Normal",cachetexture);
=======
            Texture2D cacheNormal = (Texture2D)render.material.GetTexture("_Normal");
            render.material = defaultMaterial;
            render.material.SetTexture("_Normal", cacheNormal);
>>>>>>> .merge_file_a07972
            targets.Remove(col.gameObject);
        }
    }

    public List<GameObject> GetTargets()
    {
        return targets;
    }

    void Update()
    {
        if(!PauseMenu.isPaused){
            AdjustAim();
        }
        
    }

    void AdjustAim()
    {
        // Rotate angle
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

        // Adjust Position

    }
}