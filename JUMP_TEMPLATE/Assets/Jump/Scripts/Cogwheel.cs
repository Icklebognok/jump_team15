﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cogwheel : MonoBehaviour
{
    public bool bBeingPulled = false;
    public bool bBeingPushed = false;
    JumperControl jumper;
    void Start()
    {
        jumper = GameObject.FindWithTag("Player").GetComponent<JumperControl>();
    }

    void Update()
    {
        if(!jumper.bPushPulling)
        {
            bBeingPulled = false;
            bBeingPushed = false;
        }
    }
}
