﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperControl : MonoBehaviour
{

    public List<GameObject> targets;
    public PlayerStats stats;
    Rigidbody2D targetRb;
    Rigidbody2D rb;
    private float magnitude;
    private float jumpMagnitude;
    public GameObject aimCone;
    private float moveSpeed;
    private float maxSpeed;
    public bool bPushPulling = false;
    public bool isGrounded;
    public LayerMask EnvironmentLayer;
<<<<<<< .merge_file_a02200
    private float moveX;
    private bool jumping = false;
    public float smoothMoves;
    private Vector3 Zero = Vector3.zero;
=======
    public float moveX;
    private float friction; 
    private float gravity;
    public float moveY;
>>>>>>> .merge_file_a20564
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        aimCone = GameObject.FindWithTag("Aim");
    }
    
    // Update is called once per frame
    void Update()
    {
<<<<<<< .merge_file_a02200
        smoothMoves = stats.smoothMoves;
=======
        friction = stats.friction;
        gravity = stats.gravity;
>>>>>>> .merge_file_a20564
        magnitude = stats.magnitude;
        jumpMagnitude = stats.jumpSpeed;
        moveSpeed = stats.movespeed;
        maxSpeed = stats.maxspeed;
        if (Input.GetKeyDown(KeyCode.E))
        {
            FreezeTargets();
        }        

        RaycastHit2D hit2D;
        hit2D = Physics2D.Raycast(transform.position,Vector2.up,-0.7f,EnvironmentLayer);
        Debug.DrawLine(transform.position,transform.position + new Vector3(0,-0.7f,0),hit2D ? Color.green : Color.red);
        if(hit2D){
            isGrounded = true;
        }else{
            isGrounded = false;
        }
        Move();


        


        


    }

    
    void Move()
    {
        moveX = Input.GetAxisRaw("Horizontal") * moveSpeed;
        if ((Input.GetKeyDown(KeyCode.Space)) && isGrounded)
        {
<<<<<<< .merge_file_a02200
            jumping = true;
        }
    }
    void FixedUpdate(){
        if (Input.GetMouseButton(0) && !PauseMenu.isPaused)
        {

            PushTargets();
            // Get our Rigidbody2D and set its velocity to towards the mouse at the strength of our jump
            //     GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * jumpStrength;
        }if (Input.GetMouseButton(1) && !PauseMenu.isPaused)
        {
            // Right click, pull

            // if (hit.collider != null && hit.collider.gameObject.tag != "Player")
            //  {
            //      target = hit.collider.gameObject;
            //  }
            PullTargets();
        }
        else
=======
            //move object left
            moveX -= maxSpeed * Time.deltaTime;
        }else if(Input.GetKey(KeyCode.D)){
            //move object right
            moveX += maxSpeed * Time.deltaTime;
        }else{
            if(moveX > -0.1f && moveX < 0.1f){
                moveX = 0;
            }else{
                moveX -= friction * Mathf.Sign(moveX) * Time.deltaTime;
                }
        }
        moveX = Mathf.Clamp(moveX, -maxSpeed, maxSpeed);

        RaycastHit2D hitX;
        hitX = Physics2D.BoxCast(transform.position,transform.localScale,0,Vector2.right,moveX,EnvironmentLayer);
        if(hitX){
            moveX = Mathf.Sign(moveX) * (hitX.distance -0.01f);
        }
        transform.position += new Vector3(moveX,0,0);
        moveY -= gravity * Time.deltaTime;
        if ((Input.GetKeyDown(KeyCode.Space)) && isGrounded)
>>>>>>> .merge_file_a20564
        {
            bPushPulling = false;
        }
        Vector2 TargetVelocity = new Vector2(moveX * Time.fixedDeltaTime * 10f,rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity,TargetVelocity,ref Zero,smoothMoves);
        if(jumping){
            rb.AddForce(new Vector2(0,jumpMagnitude));
            jumping = false;
        }
    }

    void PullTargets()
    {
        bPushPulling = true;
        // Pull all the targets in a cone towards  the player.
        targets = aimCone.GetComponent<JumperAim>().GetTargets();
        foreach (GameObject target in targets)
        {
            targetRb = target.GetComponent<Rigidbody2D>();
            if (target.tag == "Grappler" || target.tag == "Launchpad")
            {
                // Only trigger within certain distance
                float dist = Vector2.Distance(target.transform.position, transform.position);
                if (dist >= 1.5f)
                {
                    Vector2 directionVector = (target.transform.position - transform.position).normalized;
                    rb.AddForce(directionVector * magnitude * Time.fixedDeltaTime, ForceMode2D.Impulse);
                }
            }
            else if (target.tag == "Wheel")
            {
                target.transform.Rotate(Vector3.forward * -350.0f * Time.deltaTime);
                target.GetComponent<Cogwheel>().bBeingPulled = true;
            }
            else if (targetRb)
            {
                Vector2 directionVector = (transform.position - target.transform.position).normalized;
                targetRb.AddForce(directionVector * magnitude * Time.fixedDeltaTime, ForceMode2D.Impulse);
            }

        }
    }


    void FreezeTargets()
    {
        Debug.Log("FREEZE");
        // Freeze all destructible objects in a cone
        targets = aimCone.GetComponent<JumperAim>().GetTargets();
        foreach (GameObject target in targets)
        {
            targetRb = target.GetComponent<Rigidbody2D>();
            if (target.tag == "Destructible")
            {
                // Only trigger within certain distance
                // Toggle them to be static or dynamic
                if(!target.GetComponent<Destructible>().bFrozen)
                {
                    target.GetComponent<Destructible>().bFrozen = true;
                } else if (target.GetComponent<Destructible>().bFrozen)
                {
                    target.GetComponent<Destructible>().bFrozen = false;
                }
                
            }
        }
    }



    void PushTargets()
    {
        bPushPulling = true;
        // Pull all the targets in a cone towards  the player.
        targets = aimCone.GetComponent<JumperAim>().GetTargets();
        foreach (GameObject target in targets)
        {
            targetRb = target.GetComponent<Rigidbody2D>();
            if(target.tag == "Launchpad" || target.tag == "Grappler")
            {
                // Only triggers when the player is directly above the launchpad
                float max = target.transform.position.x + 1.0f;
                float min = target.transform.position.x - 1.0f;
                bool xAlligned = transform.position.x >= min && transform.position.x <= max;
                bool yAlligned = transform.position.y >= target.transform.position.y + 1.0f;
                if(xAlligned && yAlligned)
                {
                    Vector2 directionVector = -(target.transform.position - transform.position).normalized;
                    rb.AddForce(directionVector * magnitude * Time.fixedDeltaTime, ForceMode2D.Impulse);
                }
            }
            else if (target.tag == "Wheel")
            {
                target.transform.Rotate(Vector3.forward * 350.0f * Time.deltaTime);
                target.GetComponent<Cogwheel>().bBeingPushed = true;
            }
            else if (targetRb)
            {
                Vector2 directionVector = -(transform.position - target.transform.position).normalized;
                targetRb.AddForce(directionVector * magnitude * Time.fixedDeltaTime, ForceMode2D.Impulse);
            }

        }
    }
}
