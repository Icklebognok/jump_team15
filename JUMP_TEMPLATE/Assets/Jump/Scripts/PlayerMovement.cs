﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;
    public float horizontalMovement;
    public float speed;
    private bool jumping = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal") * speed;
        if(Input.GetKeyDown(KeyCode.Space)){
            jumping = true;
        }
    }
    void FixedUpdate(){
        controller.Move(horizontalMovement * Time.fixedDeltaTime,false,jumping);
        jumping = false;
    }
}
