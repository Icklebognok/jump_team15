﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelCreation : MonoBehaviour
{
    [Header("Arrays")]
    public GameObject[] WallObjects;
    public GameObject[] RigidObjects;
    public GameObject[] CoreObjects;
    [Header("Item Position")]
    public int ArrayList; //Which array is in use
    public int arrayIndex; //What index is in said array
    public float currentArrayLength;
    public int pieceRotation;
    [Header("CornerPieces")]
    public Transform CornerPieceA;
    public Transform CornerPieceB;
    public Vector2 CornerA;
    public Vector2 CornerB;
    [Header("Debug Testing")]
    public bool debugMode;
    public GameObject debugBlock;
    [Header("Canvas")]
    public GameObject UICanvas;
    public Image imageDisplay;
    public TextMeshProUGUI textbox;
    public GameObject selected;
    // Start is called before the first frame update
    void Start()
    {
        CornerA.x = CornerPieceB.position.x;
        CornerA.y = CornerPieceA.position.y;
        UICanvas.SetActive(false);
        CornerB.x = CornerPieceA.position.x;
        CornerB.y = CornerPieceB.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        currentArrayLength = checkArray(ArrayList);
        Vector3 mousePos = Input.mousePosition;
        Vector3 Grid = transform.position;
        mousePos.z = 0.0f;
        Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
        targetPos.z = 0.0f;
        Grid.x = Mathf.Floor(targetPos.x + 0.5f);
        Grid.y = Mathf.Floor(targetPos.y + 0.5f);
        transform.position = Grid;
        switch(ArrayList){
            case 0:
            selected = WallObjects[arrayIndex];
            break;
            case 1:
            selected = RigidObjects[arrayIndex];
            break;
            case 2:
            selected = CoreObjects[arrayIndex];
            break;
        }
        imageDisplay.sprite = selected.GetComponent<SpriteRenderer>().sprite;
        imageDisplay.transform.localEulerAngles = new Vector3(0,0,pieceRotation);
        textbox.text = selected.name.ToString();
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            if(!UICanvas.activeSelf){
                Vector3 direction = Vector2.right;
            RaycastHit2D hit = Physics2D.Raycast(Grid,direction,0.4f);
            Debug.DrawLine(Grid,Grid+direction * 0.4f,hit ? Color.green : Color.red);
            if(hit){
                Destroy(hit.transform.gameObject);
                placeObject(Grid);
            }else{
                placeObject(Grid);
            }}
            
        }
        if(Input.GetKeyDown(KeyCode.Mouse1)){
            if(!UICanvas.activeSelf){
                Vector3 direction = Vector2.right;
                RaycastHit2D hit = Physics2D.Raycast(Grid,direction,0.4f);
                Debug.DrawLine(Grid,Grid+direction * 0.4f,hit ? Color.green : Color.red);
                if(hit){
                    Destroy(hit.transform.gameObject);
                
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.A)){
            if(arrayIndex == 0){
                arrayIndex = checkArray(ArrayList);
            }else{
                arrayIndex--;
            }
        }
        if(Input.GetKeyDown(KeyCode.Z)){
            if(pieceRotation == 0){
                pieceRotation = 270;
            }else{
                pieceRotation -= 90;
            }
        }
        if(Input.GetKeyDown(KeyCode.X)){
            if(pieceRotation == 270){
                pieceRotation = 0;
            }else{
                pieceRotation += 90;
            }
        }
        if(Input.GetKeyDown(KeyCode.D)){
            if(arrayIndex == checkArray(ArrayList)){
                ArrayList = 0;
            }else{
                arrayIndex++;
            }
        }
        if(Input.GetKeyDown(KeyCode.Alpha1)){
            ArrayList = 0;
            arrayIndex = 0;
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)){
            ArrayList = 1;
            arrayIndex = 0;
        }
        if(Input.GetKeyDown(KeyCode.Alpha3)){
            ArrayList = 2;
            arrayIndex = 0;
        }
        if(Input.GetKeyDown(KeyCode.Return)){
            GenerateCode();
        }

    }
    public int checkArray(int banana){
        int returningNumber = 0;
        switch(banana){
            case 0:
                returningNumber = WallObjects.Length -1;
            break;
            case 1:
                returningNumber = RigidObjects.Length -1;
            break;
            case 2:
                returningNumber = CoreObjects.Length -1;
            break;
        }
        return returningNumber;
    }
    public void placeObject(Vector3 gridSpace){
        switch(ArrayList){
            case 0:
            Instantiate(WallObjects[arrayIndex], gridSpace,Quaternion.Euler( new Vector3(0,0,pieceRotation)));
            break;
            case 1:
            Instantiate(RigidObjects[arrayIndex], gridSpace, Quaternion.Euler( new Vector3(0,0,pieceRotation)));
            break;
            case 2:
            Instantiate(CoreObjects[arrayIndex], gridSpace, Quaternion.Euler( new Vector3(0,0,pieceRotation)));
            break;
        }
    }
    public void GenerateCode(){
        string Code = ""; //the current string of the room code
        float run = 0; //how many in a row there is the same combo
        string previousChar = ""; //what the previous character is
        int previousRot = 0; //what the previous rotation is
        for(float i = CornerB.y; i < CornerA.y+1 ; i++){
            for(float j = CornerB.x; j < CornerA.x+1; j++){
                Vector3 position = new Vector3(j,i,0);
                RaycastHit2D full = Physics2D.Raycast(position,Vector3.right,0.4f);
                if(!full){
                    if(previousChar == "X"){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                        Code += getRotation(previousRot);
                        Debug.Log(getRotation(previousRot));
                        previousChar = "X";
                        previousRot = 0;
                        run = 1;
                    }
                    if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    
                }else{
                    if(full.transform.GetComponent<WallScript>()){
                        if(previousChar == "W" && Mathf.RoundToInt(full.transform.eulerAngles.z) == previousRot){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                        Code += getRotation(previousRot);
                        Debug.Log(getRotation(previousRot));
                        previousChar = "W";
                        previousRot = Mathf.RoundToInt(full.transform.eulerAngles.z);
                        run = 1;
                        //int.Parse(getRotation(Mathf.RoundToInt(full.transform.rotation.z)))
                    }
                        if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    }
                    if(full.transform.GetComponent<PadProxy>()){
                        if(previousChar == "J" && Mathf.RoundToInt(full.transform.eulerAngles.z) == previousRot){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                        Code += getRotation(previousRot);
                        Debug.Log(getRotation(previousRot));
                        previousChar = "J";
                        previousRot = Mathf.RoundToInt(full.transform.eulerAngles.z);
                        run = 1;
                    }
                        if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    if(full.transform.GetComponent<SpikeProxy>()){
                        if(previousChar == "S" && Mathf.RoundToInt(full.transform.eulerAngles.z) == previousRot){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                        Code += getRotation(previousRot);
                        Debug.Log(getRotation(previousRot));
                        previousChar = "S";
                        previousRot = Mathf.RoundToInt(full.transform.eulerAngles.z);
                        run = 1;
                    }
                        if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    }
                    }
                    //CORE PREFABS
                    if(full.transform.GetComponent<PlayerProxy>()){
                        if(previousChar == "P" && Mathf.RoundToInt(full.transform.eulerAngles.z) == previousRot){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                        Code += getRotation(previousRot);
                        Debug.Log(getRotation(previousRot));
                        previousChar = "P";
                        previousRot = Mathf.RoundToInt(full.transform.eulerAngles.z);
                        run = 1;
                    }
                        if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    }
                    //RIGID PREFABS
                    if(full.transform.GetComponent<RigidProxy>()){
                        if(previousChar == "R" && Mathf.RoundToInt(full.transform.eulerAngles.z) == previousRot){
                        run++;
                    }else{
                        Code += run.ToString();
                        Code += previousChar;
                       Code += getRotation(previousRot);
                       Debug.Log(getRotation(previousRot));
                        previousChar = "R";
                        previousRot = Mathf.RoundToInt(full.transform.eulerAngles.z);
                        run = 1;
                    }
                        if(debugMode){
                        Instantiate(debugBlock,position,transform.rotation);
                    }
                    }
                
                }
            }
        }
            Code+= "?";
        TextEditor te = new TextEditor();
                te.text = Code;
                te.SelectAll();
                te.Copy();
                UICanvas.SetActive(true);
    }
    public void returnToMain(){
        SceneManager.LoadScene("MainScene");
    }
    public void DisableCanvas(){
        UICanvas.SetActive(false);
    }
    public string getRotation(int rotato){
        Debug.Log(rotato);
        string line = "";
        if (rotato < 0)
        {
            rotato += 360;
        }
        switch(rotato){
            case 0:
            line = "U";
            break;
            case 90:
            line = "L";
            break;
            case 180:
            line = "D";
            break;
            case 270:
            line = "R";
            break;
            default:
            line = "-";
            break;
        }
        return line;
    }
}
