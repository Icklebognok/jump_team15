﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GoToGenerate : MonoBehaviour
{
    public static string TextInput;
    public TMP_InputField textString;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void gotoMain(){
        SceneManager.LoadScene("JUMP");
    }
    public void LevelEditor(){
        SceneManager.LoadScene("LevelGen");
    }

    public void GenerateLevel(){
        TextInput = textString.text;
        SceneManager.LoadScene("CreateLevel");
    }
    public void quit(){
        Application.Quit();
    }
}
