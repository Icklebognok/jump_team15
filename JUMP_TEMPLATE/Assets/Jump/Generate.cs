﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Generate : MonoBehaviour
{
    [Header("CornerPieces")]
    public Transform CornerPieceA;
    public Transform CornerPieceB;
    public Vector2 CornerA;
    public Vector2 CornerB;
    private string RoomCode;
    private int PlaceInCode = 1;
    public GameObject[] Objects;
    public TextMeshProUGUI texbot; 
    public int[] Numbers = new int[10];
    public int Xpos = 0;
    public int Ypos = 0;
    public string inputNumber = "";
    public string inputLetter = "";
    public int inputRotation;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < Numbers.Length;i++){
            Numbers[i] = i;
        }
        //get the position of the corners
        RoomCode = GoToGenerate.TextInput;
        CornerA.x = CornerPieceB.position.x;
        CornerA.y = CornerPieceA.position.y;
        CornerB.x = CornerPieceA.position.x;
        CornerB.y = CornerPieceB.position.y;
        int testfloatX = Mathf.RoundToInt(Mathf.Abs(CornerA.x) + (Mathf.Abs(CornerA.x)*2) + CornerB.x);
        int testfloatY = Mathf.RoundToInt(Mathf.Abs(CornerA.y) + (Mathf.Abs(CornerA.y) * 2) + CornerB.y);
        GameObject[,] RoomGrid = new GameObject[testfloatX + 1,testfloatY + 1];
        string[,] ProxyGrid = new string[testfloatX + 1,testfloatY + 1];
        int[,] ProxyRotation = new int[testfloatX + 1,testfloatY + 1];
        for(int i = 2; i < RoomCode.Length; i++){
            int numbero = 0;
            string checking = RoomCode[i].ToString();
            bool Parsnip = int.TryParse(checking, out numbero);
            //Debug.Log(Parsnip);
            if(Parsnip){
                //if its a number
                inputNumber += RoomCode[i].ToString();//adding onto the number
            }else{
                //if its a letter
                int Length = 0;
                if(RoomCode[i].ToString() != "?"){
                Length = int.Parse(inputNumber);
                inputLetter = checking;
                inputRotation = getRotation(RoomCode[i+1].ToString());
                }
                i++;
                //Debug.Log(ProxyGrid.Length);
                for(int j = 0; j < Length; j++){
                    //Debug.Log(Xpos.ToString() + "," + Ypos.ToString());
                    ProxyGrid[Xpos,Ypos] = inputLetter;  
                    ProxyRotation[Xpos,Ypos] = inputRotation;                
                    if(Xpos == testfloatX){
                        Xpos = 0;
                        Ypos++;
                    }else{
                        Xpos++;
                    }
                }
                inputNumber = "0"; //reset the string of items back to 0
            }
        }
        for(float i = CornerB.y; i < CornerA.y+1; i++){
            for(float j = CornerB.x; j < CornerA.x+1; j++){
                Vector3 position = new Vector3(j,i,0);
                float GridPosX = j - CornerB.x;
                string StrPosX = GridPosX.ToString();
                int intPosX = int.Parse(StrPosX);
                float GridPosY = i - CornerB.y;
                string StrPosY = GridPosY.ToString();
                int intPosY = int.Parse(StrPosY);
               // Debug.Log(intPosX.ToString() + "," + intPosY.ToString());
                Debug.Log(ProxyRotation[intPosX,intPosY]);
                switch(ProxyGrid[intPosX,intPosY]){
                    case "W":
                        Instantiate(Objects[0],position,Quaternion.Euler( new Vector3(0,0,ProxyRotation[intPosX,intPosY])));
                    break;
                    case "P":
                        Instantiate(Objects[1],position,transform.rotation);
                    break;
                    case "R":
                        Instantiate(Objects[2],position,Quaternion.Euler( new Vector3(0,0,ProxyRotation[intPosX,intPosY])));
                    break;
                    case "J":
                        Instantiate(Objects[3],position,Quaternion.Euler(new Vector3(0,0,ProxyRotation[intPosX,intPosY])));
                    break;
                    case "S":
                        Instantiate(Objects[4],position,Quaternion.Euler(new Vector3(0,0,ProxyRotation[intPosX,intPosY])));
                    break;
                    case "X":
                    break;
                    case "?":
                    break;
                }
            }
        }
    }
       public int getRotation(string character){
            int yarr = 0;
           switch(character){
               case "U":
               yarr = 0;
               break;
               case "L":
               yarr = 90;
               break;
               case "D":
               yarr = 180;
               break;
               case "R":
               yarr = 270;
               break;
           }
           return yarr;
       }

    
}
